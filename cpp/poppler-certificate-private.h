/*
 * Copyright (C) 2009, 2011, Pino Toscano <pino@kde.org>
 * Copyright (C) 2018, Albert Astals Cid <aacid@kde.org>
 * Copyright (C) 2021, Bence Csókás <bence98@sch.bme.hu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef POPPLER_CERTIFICATE_PRIVATE_H
#define POPPLER_CERTIFICATE_PRIVATE_H

class X509CertificateInfo;

namespace poppler {

class certificate_private
{
public:
    certificate_private(const X509CertificateInfo *crt);
    ~certificate_private();

    certificate_private(const certificate_private &) = delete;
    certificate_private &operator=(const certificate_private &) = delete;

    static certificate *create(const X509CertificateInfo *cert);

    const X509CertificateInfo *cert;
};

}

#endif
