/*
 * Copyright (C) 2009-2010, Pino Toscano <pino@kde.org>
 * Copyright (C) 2021, Bence Csókás <bence98@sch.bme.hu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef POPPLER_SIGNATURE_H
#define POPPLER_SIGNATURE_H

#include "poppler-global.h"

#include <vector>

namespace poppler {

class signature_private;
class certificate;

class POPPLER_CPP_EXPORT signature : public poppler::noncopyable
{
public:
    ~signature();

    std::string signer_name() const;
    std::string subject_dn() const;
    time_type signature_time() const;
    int hash_algo() const;
    std::string signature_type() const;
    std::vector<size_t> signed_ranges() const;
    bool signature_valid() const;
    bool cert_valid() const;
    const certificate *cert_info() const;

private:
    signature(signature_private &dd);

    signature_private *d;
    friend class signature_private;
};

}

#endif
