#include "poppler-certificate.h"
#include "poppler-certificate-private.h"

#include "CertificateInfo.h"
#include "GlobalParams.h"

using namespace poppler;

certificate_private::certificate_private(const X509CertificateInfo *crt) : cert(crt) { }

certificate_private::~certificate_private() { }

certificate *certificate_private::create(const X509CertificateInfo *cert)
{
    return new certificate(*new certificate_private(cert));
}

certificate::certificate(certificate_private &dd) : d(&dd) { }

certificate::~certificate()
{
    delete d;
}

int certificate::version() const
{
    return d->cert->getVersion();
}

std::string certificate::serial() const
{
    return d->cert->getSerialNumber().toStr();
}

std::string certificate::nickname() const
{
    return d->cert->getNickName().toStr();
}

std::string certificate::issuer_cn() const
{
    return d->cert->getIssuerInfo().commonName;
}

std::string certificate::issuer_dn() const
{
    return d->cert->getIssuerInfo().distinguishedName;
}

std::string certificate::issuer_email() const
{
    return d->cert->getIssuerInfo().email;
}

std::string certificate::issuer_org() const
{
    return d->cert->getIssuerInfo().organization;
}

time_type certificate::valid_not_before() const
{
    return d->cert->getValidity().notBefore;
}

time_type certificate::valid_not_after() const
{
    return d->cert->getValidity().notAfter;
}

std::string certificate::subject_cn() const
{
    return d->cert->getSubjectInfo().commonName;
}

std::string certificate::subject_dn() const
{
    return d->cert->getSubjectInfo().distinguishedName;
}

std::string certificate::subject_email() const
{
    return d->cert->getSubjectInfo().email;
}

std::string certificate::subject_org() const
{
    return d->cert->getSubjectInfo().organization;
}

bool certificate::self_signed() const
{
    return d->cert->getIsSelfSigned();
}
