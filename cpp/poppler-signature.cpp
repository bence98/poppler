#include "SignatureInfo.h"

#include "poppler-document.h"
#include "poppler-certificate.h"
#include "poppler-certificate-private.h"
#include "poppler-signature.h"
#include "poppler-signature-private.h"

#include "PDFDoc.h"
#include "Error.h"
#include "GlobalParams.h"

using namespace poppler;

signature_private::signature_private(FormFieldSignature *fs) : formfield_sig(fs), cert(nullptr) { }

signature_private::~signature_private()
{
    if (cert)
        delete cert;
}

signature *signature_private::create(FormFieldSignature *fs)
{
    return new signature(*new signature_private(fs));
}

signature::signature(signature_private &dd) : d(&dd) { }

signature::~signature()
{
    delete d;
}

std::string signature::signer_name() const
{
    return SIGNATURE_INFO(d)->getSignerName();
}

std::string signature::subject_dn() const
{
    return SIGNATURE_INFO(d)->getSubjectDN();
}

time_type signature::signature_time() const
{
    return SIGNATURE_INFO(d)->getSigningTime();
}

int signature::hash_algo() const
{
    return SIGNATURE_INFO(d)->getHashAlgorithm();
}

std::string signature::signature_type() const
{
    switch (d->formfield_sig->getSignatureType()) {
    case adbe_pkcs7_sha1:
        return "adbe.pkcs7.sha1";
    case adbe_pkcs7_detached:
        return "adbe.pkcs7.detached";
    case ETSI_CAdES_detached:
        return "ETSI.CAdES.detached";
    default:
        return "unknown";
    }
}

std::vector<size_t> signature::signed_ranges() const
{
    std::vector<Goffset> ranges = d->formfield_sig->getSignedRangeBounds();
    std::vector<size_t> ret(ranges.begin(), ranges.end());
    Goffset checked_file_size;
    GooString *sign = d->formfield_sig->getCheckedSignature(&checked_file_size);
    if (sign) {
        ret.push_back(checked_file_size);
        delete sign;
    }
    return ret;
}

bool signature::signature_valid() const
{
    return SIGNATURE_INFO(d)->getSignatureValStatus() == SIGNATURE_VALID;
}

bool signature::cert_valid() const
{
    return SIGNATURE_INFO(d)->getCertificateValStatus() == CERTIFICATE_TRUSTED;
}

const certificate *signature::cert_info() const
{
    if (!d->cert)
        d->cert = certificate_private::create(SIGNATURE_INFO(d)->getCertificateInfo());
    return d->cert;
}
