/*
 * Copyright (C) 2009-2010, Pino Toscano <pino@kde.org>
 * Copyright (C) 2021, Bence Csókás <bence98@sch.bme.hu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef POPPLER_CERTIFICATE_H
#define POPPLER_CERTIFICATE_H

#include "poppler-global.h"

#include <vector>

namespace poppler {

class certificate_private;

class POPPLER_CPP_EXPORT certificate : public poppler::noncopyable
{
public:
    ~certificate();

    int version() const;
    std::string serial() const;
    std::string nickname() const;
    std::string issuer_cn() const;
    std::string issuer_dn() const;
    std::string issuer_email() const;
    std::string issuer_org() const;
    time_type valid_not_before() const;
    time_type valid_not_after() const;
    std::string subject_cn() const;
    std::string subject_dn() const;
    std::string subject_email() const;
    std::string subject_org() const;
    bool self_signed() const;

private:
    certificate(certificate_private &dd);

    certificate_private *d;
    friend class certificate_private;
};

}

#endif
