/*
 * Copyright (C) 2009, 2011, Pino Toscano <pino@kde.org>
 * Copyright (C) 2018, Albert Astals Cid <aacid@kde.org>
 * Copyright (C) 2021, Bence Csókás <bence98@sch.bme.hu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef POPPLER_SIGNATURE_PRIVATE_H
#define POPPLER_SIGNATURE_PRIVATE_H

#define SIGNATURE_INFO(d) ((d)->formfield_sig->validateSignature(true, false, -1 /* now */))

class FormFieldSignature;

namespace poppler {

class certificate;

class signature_private
{
public:
    signature_private(FormFieldSignature *fs);
    ~signature_private();

    signature_private(const signature_private &) = delete;
    signature_private &operator=(const signature_private &) = delete;

    static signature *create(FormFieldSignature *fs);

    FormFieldSignature *formfield_sig;
    certificate *cert;
};

}

#endif
